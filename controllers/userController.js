const { User } = require('../models/userModel');

module.exports.getUsersById = async (req, res) => {
    const user = await User.findById(req.params.id);

    if(!user) {
        return res.status(400).json({message: `No user with id ${req.params.id} found!`})
    }

    res.json({user});
};

module.exports.modifyUser = async (req, res) => {
    await User.findByIdAndUpdate(req.params.id, req.body);
    res.json({status: 'success'});
};

module.exports.deleteUser = async (req, res) => {
    await User.findByIdAndDelete(req.params.id);
    res.json({status: 'success'});
};