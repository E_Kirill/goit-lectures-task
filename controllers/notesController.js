const { Note } = require('../models/notesModel');

// db.users.aggregate([
//         {
//             $project: {
//                 "_id": {
//                     "$toString": "$_id"
//                 },
//                 "username": 1,
//                 "password": 1,
//                 "nicknames": 1,
//                 "email": 1,
//             }
//         },
//         {
//             $lookup:
//               {
//                 from: 'notes',
//                 localField: '_id',
//                 foreignField: 'userId',
//                 as: 'user_notes'
//               }
//          },
//          {
//             $project: {
//                 "_id": 1,
//                 "username": 1,
//                 "password": 1,
//                 "nicknames": 1,
//                 "email": 1,
//                 "user_notes": 1,
//                 "user_notes_count": { $size: "$user_notes" },
//             }
//         },
//     ]); 

module.exports.getNotes = async (req, res) => {
    const limit = req.query.limit ? parseInt(req.query.limit) : 5;
    const skip = req.query.skip ? parseInt(req.query.skip) : 0;
    const sortOrder = req.query.sort == 'ASC' ? 1 : -1;

    const notes = await Note.find(
        { userId: req.user._id },
        { __v: 0 },
        { skip, limit, sort: {'text': sortOrder} }
    );

    res.json({notes, skip, limit});
};

module.exports.getNoteById = async (req, res) => {

};

module.exports.addNote = async (req, res) => {
    const note = new Note({text: req.body.text, userId: req.user._id});
    await note.save();

    res.json({message: 'Note created successfully!'});
};

module.exports.updateNote = async (req, res) => {

};

module.exports.deleteNote = async (req, res) => {

};