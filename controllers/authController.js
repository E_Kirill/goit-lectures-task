const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

module.exports.registration = async (req, res) => {
    const user = new User({
        username: req.body.username,
        password: await bcrypt.hash(req.body.password, 10),
        email: req.body.email,
        nicknames: req.body.nicknames,
    });

    await user.save();

    res.json({message: 'Successfull registration!'});
};

module.exports.login = async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(401).json({message: `No users with username '${req.body.username}' found!`})
    }

    if (!(await bcrypt.compare(req.body.password, user.password))) {
        return res.status(401).json({message: `Wrong password!`})
    }

    const token = jwt.sign({
        _id: user._id,
        username: user.username,
        email: user.email,
    }, process.env.JWT_SECRET);

    res.json({message: 'Successfull authorization', token});
};