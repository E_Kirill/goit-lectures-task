const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const {
    login,
    registration
} = require('../controllers/authController');

router.post('/registration', asyncWrapper(registration));
router.post('/login', asyncWrapper(login));

module.exports = router;