const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const {
    getUsersById,
    modifyUser,
    deleteUser
 } = require('../controllers/userController');

router.get('/:id', asyncWrapper(getUsersById));
router.put('/:id', asyncWrapper(modifyUser));
router.delete('/:id', asyncWrapper(deleteUser));

module.exports = router;