const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const {
    getNotes,
    getNoteById,
    addNote,
    updateNote,
    deleteNote
} = require('../controllers/notesController');
const { authorizeUser } = require('./middlewares/authMiddleware');

router.get('/', authorizeUser, asyncWrapper(getNotes));
router.get('/:id', authorizeUser, asyncWrapper(getNoteById));
router.post('/', authorizeUser, asyncWrapper(addNote));
router.put('/:id', authorizeUser, asyncWrapper(updateNote));
router.delete('/:id', authorizeUser, asyncWrapper(deleteNote));

module.exports = router;