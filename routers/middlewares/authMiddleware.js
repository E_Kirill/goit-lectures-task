const jwt = require('jsonwebtoken');

module.exports.authorizeUser = async (req, res, next) => {
    // headers -> authorization: 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..6i0eAdvE_o9yV_qZPU4vJ9VFw343klLYewlVBStMBHw'
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
        res.status(401).json({message: 'No authorization header found!'});
    }

    const [tokenType, jwtToken] = authHeader.split(' ');

    if (!jwtToken) {
        res.status(401).json({message: 'No JWT token found in header!'});
    }

    req.user = jwt.verify(jwtToken, process.env.JWT_SECRET);
    req.token = jwtToken;
    next();
}