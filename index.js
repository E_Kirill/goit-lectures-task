const mongoose = require('mongoose');
const express = require('express');
const app = express();

const usersRouter = require('./routers/usersRouter');
const authRouter = require('./routers/authRouter');
const notesRouter = require('./routers/notesRouter');

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

app.use((error, req, res, next) => {
    res.status(500).json({message: error.message})
});

async function start () {
    try {
        require('dotenv').config()

        await mongoose.connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        });

        app.listen(process.env.PORT || 8080);
    } catch (err) {
        console.log('Start up error: ', err);
    }
}

start();

